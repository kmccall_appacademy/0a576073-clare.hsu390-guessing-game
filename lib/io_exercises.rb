def guessing_game
    answer = (1..100).to_a.sample
    print "Guess a number between 1 and 100: "
    guesses = 0

    $stdin.each do |input|
      guesses += 1
      if input.to_i > answer
        print "#{input} was too high. Try again: "
      elsif input.to_i < answer
        print "#{input} was too low. Try again: "
      elsif input.to_i == answer
        print "The number was #{answer}. It took you #{guesses} tries."
        break
      end
    end
end
